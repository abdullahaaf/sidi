-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 13, 2016 at 05:57 
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_diri`
--

CREATE TABLE IF NOT EXISTS `data_diri` (
  `nim` varchar(10) NOT NULL DEFAULT '',
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tanggal_lahir` varchar(50) DEFAULT NULL,
  `kebangsaan` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `hobi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_anatomi`
--

CREATE TABLE IF NOT EXISTS `foto_anatomi` (
  `nim` varchar(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_biometrik`
--

CREATE TABLE IF NOT EXISTS `foto_biometrik` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_ciri_fisik`
--

CREATE TABLE IF NOT EXISTS `foto_ciri_fisik` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_dokumen_pribadi`
--

CREATE TABLE IF NOT EXISTS `foto_dokumen_pribadi` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_fotografi`
--

CREATE TABLE IF NOT EXISTS `foto_fotografi` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_kebiasaan_makan`
--

CREATE TABLE IF NOT EXISTS `foto_kebiasaan_makan` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_kepala`
--

CREATE TABLE IF NOT EXISTS `foto_kepala` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_pendidikan`
--

CREATE TABLE IF NOT EXISTS `foto_pendidikan` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_riwayat_penyakit`
--

CREATE TABLE IF NOT EXISTS `foto_riwayat_penyakit` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_teman_dekat`
--

CREATE TABLE IF NOT EXISTS `foto_teman_dekat` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suara_musik_fav`
--

CREATE TABLE IF NOT EXISTS `suara_musik_fav` (
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `text_makan`
--

CREATE TABLE IF NOT EXISTS `text_makan` (
  `nim` int(11) NOT NULL,
  `nama_menu_sarapan` varchar(30) DEFAULT NULL,
  `nama_menu_makan_siang` varchar(30) DEFAULT NULL,
  `nama_menu_malam` varchar(30) DEFAULT NULL,
  `makanan_favorit` varchar(30) DEFAULT NULL,
  `cemilan_favorit` varchar(30) DEFAULT NULL,
  `tempat_makan` varchar(30) DEFAULT NULL,
  `alamat_warung` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `text_pendidikan`
--

CREATE TABLE IF NOT EXISTS `text_pendidikan` (
  `nama_mi` varchar(20) NOT NULL,
  `alamat_mi` varchar(100) DEFAULT NULL,
  `thn_lulus_mi` varchar(100) DEFAULT NULL,
  `nama_smp` varchar(100) DEFAULT NULL,
  `alamat_smp` varchar(100) DEFAULT NULL,
  `thn_lulus_smp` varchar(20) DEFAULT NULL,
  `nama_sma` varchar(100) DEFAULT NULL,
  `alamat_sma` varchar(20) DEFAULT NULL,
  `thn_lulus_sma` varchar(100) DEFAULT NULL,
  `nama_pt` varchar(20) DEFAULT NULL,
  `alamat_pt` varchar(50) DEFAULT NULL,
  `thn_lulus` varchar(50) DEFAULT NULL,
  `nim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `text_riwayat_penyakit`
--

CREATE TABLE IF NOT EXISTS `text_riwayat_penyakit` (
  `nim` int(11) NOT NULL,
  `gol_darah` char(5) DEFAULT NULL,
  `penyakit_yg_pernah_diderita` varchar(100) DEFAULT NULL,
  `penyakit_sedang_diderita` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `text_teman`
--

CREATE TABLE IF NOT EXISTS `text_teman` (
  `id_teman` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `namaTeman` varchar(100) NOT NULL,
  `jenisKelamin` varchar(1) NOT NULL,
  `alamatTeman` text NOT NULL,
  `tempatLahir` varchar(100) NOT NULL,
  `tanggalLahir` date NOT NULL,
  `hpTeman` varchar(13) NOT NULL,
  `emailTeman` varchar(100) NOT NULL,
  `sebagaiTeman` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `url` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ukuran` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_diri`
--
ALTER TABLE `data_diri`
  ADD PRIMARY KEY (`nim`),
  ADD KEY `hobi` (`hobi`);

--
-- Indexes for table `foto_anatomi`
--
ALTER TABLE `foto_anatomi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_biometrik`
--
ALTER TABLE `foto_biometrik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_ciri_fisik`
--
ALTER TABLE `foto_ciri_fisik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_dokumen_pribadi`
--
ALTER TABLE `foto_dokumen_pribadi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_fotografi`
--
ALTER TABLE `foto_fotografi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_kebiasaan_makan`
--
ALTER TABLE `foto_kebiasaan_makan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_kepala`
--
ALTER TABLE `foto_kepala`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_pendidikan`
--
ALTER TABLE `foto_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_riwayat_penyakit`
--
ALTER TABLE `foto_riwayat_penyakit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foto_teman_dekat`
--
ALTER TABLE `foto_teman_dekat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suara_musik_fav`
--
ALTER TABLE `suara_musik_fav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text_makan`
--
ALTER TABLE `text_makan`
  ADD PRIMARY KEY (`nim`),
  ADD KEY `sarap_idx` (`nama_menu_sarapan`),
  ADD KEY `diner_idx` (`nama_menu_makan_siang`);

--
-- Indexes for table `text_pendidikan`
--
ALTER TABLE `text_pendidikan`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `text_riwayat_penyakit`
--
ALTER TABLE `text_riwayat_penyakit`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `text_teman`
--
ALTER TABLE `text_teman`
  ADD PRIMARY KEY (`id_teman`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `foto_anatomi`
--
ALTER TABLE `foto_anatomi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_biometrik`
--
ALTER TABLE `foto_biometrik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_ciri_fisik`
--
ALTER TABLE `foto_ciri_fisik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_dokumen_pribadi`
--
ALTER TABLE `foto_dokumen_pribadi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_fotografi`
--
ALTER TABLE `foto_fotografi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_kebiasaan_makan`
--
ALTER TABLE `foto_kebiasaan_makan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_kepala`
--
ALTER TABLE `foto_kepala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_pendidikan`
--
ALTER TABLE `foto_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_riwayat_penyakit`
--
ALTER TABLE `foto_riwayat_penyakit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `foto_teman_dekat`
--
ALTER TABLE `foto_teman_dekat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suara_musik_fav`
--
ALTER TABLE `suara_musik_fav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `text_teman`
--
ALTER TABLE `text_teman`
  MODIFY `id_teman` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
