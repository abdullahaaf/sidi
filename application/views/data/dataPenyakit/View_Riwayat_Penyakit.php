
  <div class="box box-primary">
    <div class="box-header with-border">
    <h3>Data Riwayat Penyakit Anda</h3>
    <br />
    <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Riwayat</button>
    <br />
    <br />
    <div >
    <table id="table" class="table table-striped">
      <tr>
        <td><span><b>Golongan Darah </b></span></td>
        <td><span id="vgol_darah"></span></td>
      </tr>
      <tr>
        <td><span><b>Penyakit yang Pernah Diderita </b></span></td>
        <td><span id="vpenyakit_yg_pernah_diderita"></span></td>
      </tr>
      <tr>
        <td><span><b>Penyakit yang Sedang Diderita </b></span></td>
        <td><span id="vpenyakit_sedang_diderita"></span></td>
      </tr>
    </table>
      
    </div>
    </div>
  </div>    
<!-- ============ batas tampil ============ -->

  <script type="text/javascript">

    var save_method; //for save method string
    $(document).ready(function() {
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/dataPenyakit/Con_Riwayat_Penyakit/ajax_edit/')?>/" + "<?php echo $id;?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           var tombol;
           var ifid='<?php echo $id?>';
           if(ifid!=""){
            tombol= '<button class="btn btn-warning" onclick="edit_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-pencil"></i> Edit Data</button> | <button class="btn btn-danger" onclick="delete_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-trash"></i> Hapus Data</button>';
              $('#tomboldiv').html(tombol);
            }

            $('#vgol_darah').html(data.gol_darah);
            $('#vpenyakit_yg_pernah_diderita').html(data.penyakit_yg_pernah_diderita);
            $('#vpenyakit_sedang_diderita').html(data.penyakit_sedang_diderita);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          
        }
    });
    });

    function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Riwayat'); // Set Title to Bootstrap modal title
    }

    function edit_person(id_riwayat)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/dataPenyakit/Con_Riwayat_Penyakit/ajax_edit/')?>/" + id_riwayat,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           
            $('[name="id_riwayat"]').val(data.id_riwayat);
            $('[name="gol_darah"]').val(data.gol_darah);
            $('[name="penyakit_yg_pernah_diderita"]').val(data.penyakit_yg_pernah_diderita);
            $('[name="penyakit_sedang_diderita"]').val(data.penyakit_sedang_diderita);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Riwayat'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo site_url('data/dataPenyakit/Con_Riwayat_Penyakit/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('data/dataPenyakit/Con_Riwayat_Penyakit/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(id_riwayat)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('data/dataPenyakit/Con_Riwayat_Penyakit/ajax_delete')?>/"+id_riwayat,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Data Riwayat Penyakit</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">         
          <input type="hidden" value="<?php echo $id;?>" name="nim"/> 
          <div class="form-body">
              
             <div class="form-group">           
              <label class="control-label col-md-3">Golongan Darah</label>
              <div class="col-md-9">
                <input name="gol_darah" placeholder="Golongan Darah" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Penyakit yang Pernah Diderita</label>
              <div class="col-md-9">
                <input name="penyakit_yg_pernah_diderita" placeholder="Penyakit yang Pernah Diderita" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Penyakit yang Sedang DIderita</label>
              <div class="col-md-9">
                <input name="penyakit_sedang_diderita" placeholder="Penyakit yang Sedang DIderita" class="form-control" type="text">
              </div>
            </div>
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->