
  <div class="box box-primary" style="overflow-x:scroll;">
    <div class="box-header with-border">
    <h1>Selamat Datang di Menu Data Diri</h1>
    <h3>Data Diri Anda</h3>
    <br />
    <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br />
    <br />
    <div >
    <table id="table" class="table table-striped">
      <tr>
        <td><span><b>Nama SD</b></span></td>
        <td><span id="vnama_mi"></span></td>
      </tr>
      <tr>
        <td><span><b>Alamat SD</b></span></td>
        <td><span id="valamat_mi"></span></td>
      </tr>
      <tr>
        <td><span><b>Tahun Lulus SD</b></span></td>
        <td><span id="vthn_lulus_mi"></span></td>
      </tr>
      <tr>
        <td><span><b>Nama SMP</b></span></td>
        <td><span id="vnama_smp"></span></td>
      </tr>
      <tr>
        <td><span><b>Alamat SMP</b></span></td>
        <td><span id="valamat_smp"></span></td>
      </tr>
      <tr>
        <td><span><b>Tahun Lulus SMP </b></span></td>
        <td><span id="vthn_lulus_smp"></span></td>
      </tr>
      <tr>
        <td><span><b>Nama SMA </b></span></td>
        <td><span id="vnama_sma"></span></td>
      </tr>
      <tr>
        <td><span><b>Alamat SMA </b></span></td>
        <td><span id="valamat_sma"></span></td>
      </tr>
      <tr>
        <td><span><b>Tahun Lulus SMA </b></span></td>
        <td><span id="vthn_lulus_sma"></span></td>
      </tr>
      <tr>
        <td><span><b>Nama Perguruan Tinggi</b> </span></td>
        <td><span id="vnama_pt"></span></td>
      </tr>
      <tr>
        <td><span><b>Alamat Pergururan Tinggi </b></span></td>
        <td><span id="valamat_pt"></span></td>
      </tr>
      <tr>
        <td><span><b>Tahun Lulus </b></span></td>
        <td><span id="vthn_lulus"></span></td>
      </tr>

    </table>
      
    </div>
    </div>
  </div>    
<!-- ============ batas tampil ============ -->

  <script type="text/javascript">

    var save_method; //for save method string
    var table;
    $(document).ready(function() {
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/datapendidikan/Con_Master_Data_Pendidikan/ajax_edit/')?>/" + "<?php echo $id;?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           var tombol;
           var ifid='<?php echo $id?>';
           if(ifid!=""){
            tombol= '<button class="btn btn-warning" onclick="edit_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-pencil"></i> Edit Data</button> | <button class="btn btn-danger" onclick="delete_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-trash"></i> Hapus Data</button>';
              $('#tomboldiv').html(tombol);
            }

            $('#vnama_mi').html(data.nama_mi);
            $('#valamat_mi').html(data.alamat_mi);
            $('#vthn_lulus_mi').html(data.thn_lulus_mi);
            $('#vnama_smp').html(data.nama_smp);
            $('#valamat_smp').html(data.alamat_smp);
            $('#vthn_lulus_smp').html(data.thn_lulus_smp);
            $('#vnama_sma').html(data.nama_sma);
            $('#valamat_sma').html(data.alamat_sma);
            $('#vthn_lulus_sma').html(data.thn_lulus_sma);
            $('#vnama_pt').html(data.nama_pt);
            $('#valamat_pt').html(data.alamat_pt);
            $('#vthn_lulus').html(data.thn_lulus);
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
        }
    });
    });

    function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Data'); // Set Title to Bootstrap modal title
    }

    function edit_person(id_pendidikan)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/datapendidikan/Con_Master_Data_Pendidikan/ajax_edit/')?>/" + id_pendidikan,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           
            $('[name="id_pendidikan"]').val(data.id_pendidikan);
            $('[name="nama_mi"]').val(data.nama_mi);
            $('[name="alamat_mi"]').val(data.alamat_mi);
            $('[name="thn_lulus_mi"]').val(data.thn_lulus_mi);
            $('[name="nama_smp"]').val(data.nama_smp);
            $('[name="alamat_smp"]').val(data.alamat_smp);
            $('[name="thn_lulus_smp"]').val(data.thn_lulus_smp);
             $('[name="nama_sma"]').val(data.nama_sma);
            $('[name="alamat_sma"]').val(data.alamat_sma);
            $('[name="thn_lulus_sma"]').val(data.thn_lulus_sma);
             $('[name="nama_pt"]').val(data.nama_pt);
            $('[name="alamat_pt"]').val(data.alamat_pt);
            $('[name="thn_lulus"]').val(data.thn_lulus);
            // $('[name="address"]').val(data.address);
            // $('[name="dob"]').val(data.dob);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo site_url('data/datapendidikan/Con_Master_Data_Pendidikan/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('data/datapendidikan/Con_Master_Data_Pendidikan/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(id_pendidikan)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('data/datapendidikan/Con_Master_Data_Pendidikan/ajax_delete')?>/"+id_pendidikan,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Data Riwayat Pendidikan </h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">         
          <input type="hidden" value="<?php echo $id;?>" name="nim"/> 
          <div class="form-body">
              
             <div class="form-group">           
              <label class="control-label col-md-3">Nama SD/MI</label>
              <div class="col-md-9">
                <input name="nama_mi" placeholder="nama" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat SD/MI</label>
              <div class="col-md-9">
                <input name="alamat_mi" placeholder="alamat mi/sd" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Tahun Lulus SD/MI</label>
              <div class="col-md-9">
                <input name="thn_lulus_mi" placeholder="Tahun Lulus SD" class="form-control" type="text">
              </div>
            </div>

                    <div class="form-group">           
              <label class="control-label col-md-3">Nama SMP/MTs</label>
              <div class="col-md-9">
                <input name="nama_smp" placeholder="nama SMP/MTs" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat SMP/MTs</label>
              <div class="col-md-9">
                <input name="alamat_smp" placeholder="alamat smp/mts" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Tahun Lulus SMP/MTs</label>
              <div class="col-md-9">
                <input name="thn_lulus_smp" placeholder="Tahun Lulus SD" class="form-control" type="text">
              </div>
            </div>

                    <div class="form-group">           
              <label class="control-label col-md-3">Nama SMA/MA</label>
              <div class="col-md-9">
                <input name="nama_sma" placeholder="nama sma" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat SMA/MA</label>
              <div class="col-md-9">
                <input name="alamat_sma" placeholder="alamat sma/ma" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Tahun Lulus SMA/MA</label>
              <div class="col-md-9">
                <input name="thn_lulus_sma" placeholder="Tahun Lulus SMA" class="form-control" type="text">
              </div>
            </div>
              
                   <div class="form-group">           
              <label class="control-label col-md-3">Nama PT</label>
              <div class="col-md-9">
                <input name="nama_pt" placeholder="nama pt" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat PT</label>
              <div class="col-md-9">
                <input name="alamat_pt" placeholder="alamat pt" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Tahun Lulus PT</label>
              <div class="col-md-9">
                <input name="thn_lulus" placeholder="Tahun Lulus PT" class="form-control" type="text">
              </div>
            </div>
             
<!--
            <div class="form-group">
              <label class="control-label col-md-3"></label>
              <div class="col-md-9">
                <select name="keadaan" class="form-control">
                  <option value="baik">Baik</option>
                  <option value="rusak">Rusak</option>
                  <option value="kurang">Kurang</option>
                </select>
              </div>
            </div>  
-->

           <!--  <div class="form-group">
              <label class="control-label col-md-3">Date of Birth</label>
              <div class="col-md-9">
                <!-- <input name="dob" placeholder="yyyy-mm-dd" class="form-control" type="text"> -->
                                  <!-- Date dd/mm/yyyy -->
                    <!-- <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="dob" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>
                    </div><!-- /.input group --> 

             <!--  </div>
            </div> -->
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->