  <div class="box box-primary">
    <div class="box-header with-border">
    <h1>Selamat Datang di Menu Data Diri</h1>
    <h3>Data Diri Anda</h3>
    <br />
    <div id="tomboldiv">
      <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    </div>

    <br />
    <br />

    <div >
    <table id="table" class="table table-striped">
      <tr>
        <td><span><b>NIM </b></span></td>
        <td><span id="vnim"></span></td>
      </tr>
      <tr>
        <td><span><b>Nama </b></span></td>
        <td><span id="vnama"></span></td>
      </tr>
      <tr>
        <td><span><b>Alamat </b></span></td>
        <td><span id="valamat"></span></td>
      </tr>
      <tr>
        <td><span><b>Telepon </b></span></td>
        <td><span id="vtelepon"></span></td>
      </tr>
      <tr>
        <td><span><b>Hp </b></span></td>
        <td><span id="vhp"></span></td>
      </tr>
      <tr>
        <td><span><b>Email </b></span></td>
        <td><span id="vemail"></span></td>
      </tr>
      <tr>
        <td><span><b>Tanggal Lahir </b></span></td>
        <td><span id="vtanggal_lahir"></span></td>
      </tr>
      <tr>
        <td><span><b>Kebangsaan </b></span></td>
        <td><span id="vkebangsaan"></span></td>
      </tr>
      <tr>
        <td><span><b>Agama </b></span></td>
        <td><span id="vagama"></span></td>
      </tr>
      <tr>
        <td><span><b>Jenis Kelamin</b> </span></td>
        <td><span id="vjenis_kelamin"></span></td>
      </tr>
      <tr>
        <td><span><b>Status </b></span></td>
        <td><span id="vstatus"></span></td>
      </tr>
      <tr>
        <td><span><b>Hobi </b></span></td>
        <td><span id="vhobi"></span></td>
      </tr>

    </table>
      
    </div>
    
    </div>
  </div>    
<!-- ============ batas tampil ============ -->

  <script type="text/javascript">

    var save_method; //for save method string
    $(document).ready(function() {
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/datadiri/Con_Master_Data_Datadiri/ajax_edit/')?>/" + "<?php echo $id;?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           var tombol;
           var ifid='<?php echo $id?>';
           if(ifid!=""){
            tombol= '<button class="btn btn-warning" onclick="edit_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-pencil"></i> Edit Data</button> | <button class="btn btn-danger" onclick="delete_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-trash"></i> Hapus Data</button>';
              $('#tomboldiv').html(tombol);
            }

            $('#vnim').html(data.nim);
            $('#vnama').html(data.nama);
            $('#valamat').html(data.alamat);
            $('#vtelepon').html(data.telepon);
            $('#vhp').html(data.hp);
            $('#vemail').html(data.email);
            $('#vtanggal_lahir').html(data.tanggal_lahir);
            $('#vkebangsaan').html(data.kebangsaan);
            $('#vagama').html(data.agama);
            $('#vjenis_kelamin').html(data.jenis_kelamin);
            $('#vstatus').html(data.status);
            $('#vhobi').html(data.hobi);
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    });

    function add_person()
    {
            var rod= '<label class="control-label col-md-3">NIM</label><div class="col-md-9"><input name="nim" placeholder="NIM" class="form-control" type="text"></div>';
            $('#fnim').html(rod);
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Data'); // Set Title to Bootstrap modal title
    }

    function edit_person(nim)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/datadiri/Con_Master_Data_Datadiri/ajax_edit/')?>/" + nim,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            if(data.nim!=""){
            var ro = '<label class="control-label col-md-3">NIM</label><div class="col-md-9"><input name="nim" readonly="readonly" placeholder="NIM" class="form-control" type="text"></div>';
            $('#fnim').html(ro);
            }
            else{
            var rod= '<label class="control-label col-md-3">NIM</label><div class="col-md-9"><input name="nim" placeholder="NIM" class="form-control" type="text"></div>';
            $('#fnim').html(rod);
            }

            $('[name="nim"]').val(data.nim);
            $('[name="nama"]').val(data.nama);
            $('[name="alamat"]').val(data.alamat);
            $('[name="telepon"]').val(data.telepon);
            $('[name="hp"]').val(data.hp);
            $('[name="email"]').val(data.email);
            $('[name="tanggal_lahir"]').val(data.tanggal_lahir);
            $('[name="kebangsaan"]').val(data.kebangsaan);
            $('[name="agama"]').val(data.agama);
            $('[name="jenis_kelamin"]').val(data.jenis_kelamin);
            $('[name="status"]').val(data.status);
            $('[name="hobi"]').val(data.hobi);
            // $('[name="address"]').val(data.address);
            // $('[name="dob"]').val(data.dob);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      var table;
      table = $('#table');
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
        url = "<?php echo site_url('data/datadiri/Con_Master_Data_Datadiri/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('data/datadiri/Con_Master_Data_Datadiri/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(nim)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('data/datadiri/Con_Master_Data_Datadiri/ajax_delete')?>/"+nim,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Data Diri</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">         
          <!-- <input type="hidden" value="" name="id_datadiri"/>  -->
          <div class="form-body">
            <div id="fnim" class="form-group">           
              
            </div>

             <div class="form-group">           
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-9">
                <input name="nama" placeholder="nama" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat</label>
              <div class="col-md-9">
                <input name="alamat" placeholder="alamat" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Telepon</label>
              <div class="col-md-9">
                <input name="telepon" placeholder="telepon" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">
              <label class="control-label col-md-3">Hp</label>
              <div class="col-md-9">
                <input name="hp" placeholder="hp" class="form-control" type="text">
              </div>
              </div>
                
                <div class="form-group">
              <label class="control-label col-md-3">email</label>
              <div class="col-md-9">
                <input name="email" placeholder="email" class="form-control" type="text">
              </div>
            </div>             
               <div class="form-group">
              <label class="control-label col-md-3">Tanggal Lahir</label>
              <div class="col-md-9">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="tanggal_lahir" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>
                    </div><!-- /.input group -->

              </div>
            </div>
                
                <div class="form-group">
              <label class="control-label col-md-3">Kebangsaan</label>
              <div class="col-md-9">
                <input name="kebangsaan" placeholder="kebangsaan" class="form-control" type="text">
              </div>
            </div>
                <div class="form-group">
              <label class="control-label col-md-3">Agama</label>
              <div class="col-md-9">
                <input name="agama" placeholder="agama" class="form-control" type="text">
              </div>
            </div>
                
                <div class="form-group">
              <label class="control-label col-md-3">Jenis Kelamin</label>
              <div class="col-md-9">
                <select name="jenis_kelamin" class="form-control">
                   <option value="laki-laki">Laki laki</option>
                   <option value="perempuan">Perempuan</option></select>
                  
                 
              </div>
            </div>
              
              <div class="form-group">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-9">
               <input name="status" placeholder="Status" class="form-control" type="text">
              </div>
            </div>
              
                <div class="form-group">
              <label class="control-label col-md-3">Hobi</label>
              <div class="col-md-9">
               <input name="hobi" placeholder="Hobi" class="form-control" type="text">
              </div>
            </div>
             
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->