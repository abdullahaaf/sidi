
    <script src="<?=base_url('assets/html5gallery/jquery.js')?>"></script>
    <script src="<?=base_url('assets/html5gallery/html5gallery.js')?>"></script>

<div class="box box-primary">
    <div class="box-header with-border">
    <h1>Gallery Video</h1>
    <br />
    <br />

        <div class="row">
        <?php 
        if($video!=null){
            foreach($video as $rowprov){
              
                echo '<div class="col-md-6">';
                echo '<video width="100%" height="100%" controls>';
                echo '<source src="'.base_url("assets/uploads/$rowprov->url").'" type="'.$rowprov->type.'" />';
                echo '</video>';
                echo '<b><h5>'.$rowprov->title.'</h5></b>';
                echo '</div>';  
              }
        }
        else{
            echo '<div class="col-md-6">';
            echo '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong><center>Bahaya!</center></strong><center><h1>Video Kosong</h1></center></div>';
            echo '</div>';  
        }
              
            
        ?>
        </div> 

     </div>
  </div>  