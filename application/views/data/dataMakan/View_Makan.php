
  <div class="box box-primary">
    <div class="box-header with-border">
    <h3>Data Kebiasaan Makan</h3>
    <br />
    <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Kebiasaan</button>
    <br />
    <br />
    <div >
    <table id="table" class="table table-striped">
      <tr>
        <td><span><b>Nama Menu Sarapan </b></span></td>
        <td><span id="vnama_menu_sarapan"></span></td>
      </tr>
      <tr>
        <td><span><b>Nama Menu Makan Siang </b></span></td>
        <td><span id="vnama_menu_makan_siang"></span></td>
      </tr>
      <tr>
        <td><span><b>Nama Menu Malam </b></span></td>
        <td><span id="vnama_menu_malam"></span></td>
      </tr>
      <tr>
        <td><span><b>Makanan Favorit </b></span></td>
        <td><span id="vmakanan_favorit"></span></td>
      </tr>
      <tr>
        <td><span><b>Cemilan Favorit </b></span></td>
        <td><span id="vcemilan_favorit"></span></td>
      </tr>
      <tr>
        <td><span><b>Tempat Makan </b></span></td>
        <td><span id="vtempat_makan"></span></td>
      </tr>
      <tr>
        <td><span><b>Alamat Warung </b></span></td>
        <td><span id="valamat_warung"></span></td>
      </tr>
    </table>
      
    </div>
    </div>
  </div>    
<!-- ============ batas tampil ============ -->

  <script type="text/javascript">

    var save_method; //for save method string
    $(document).ready(function() {
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/dataMakan/Con_Makan/ajax_edit/')?>/" + "<?php echo $id;?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           var tombol;
           var ifid='<?php echo $id?>';
           if(ifid!=""){
            tombol= '<button class="btn btn-warning" onclick="edit_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-pencil"></i> Edit Data</button> | <button class="btn btn-danger" onclick="delete_person('+'<?php echo $id;?>'+')"><i class="glyphicon glyphicon-trash"></i> Hapus Data</button>';
              $('#tomboldiv').html(tombol);
            }

            $('#vnama_menu_sarapan').html(data.nama_menu_sarapan);
            $('#vnama_menu_makan_siang').html(data.nama_menu_makan_siang);
            $('#vnama_menu_malam').html(data.nama_menu_malam);
            $('#vmakanan_favorit').html(data.makanan_favorit);
            $('#vcemilan_favorit').html(data.cemilan_favorit);
            $('#vtempat_makan').html(data.tempat_makan);
            $('#valamat_warung').html(data.alamat_warung);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
        }
    });
    });

    function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Kebiasaan'); // Set Title to Bootstrap modal title
    }

    function edit_person(id_makan)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/dataMakan/Con_Makan/ajax_edit/')?>/" + id_makan,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           
            $('[name="id_makan"]').val(data.id_makan);
            $('[name="nama_menu_sarapan"]').val(data.nama_menu_sarapan);
            $('[name="nama_menu_makan_siang"]').val(data.nama_menu_makan_siang);
            $('[name="nama_menu_malam"]').val(data.nama_menu_malam);
            $('[name="makanan_favorit"]').val(data.makanan_favorit);
            $('[name="cemilan_favorit"]').val(data.cemilan_favorit);
            $('[name="tempat_makan"]').val(data.tempat_makan);
            $('[name="alamat_warung"]').val(data.alamat_warung);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Kebiasaan'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo site_url('data/dataMakan/Con_Makan/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('data/dataMakan/Con_Makan/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(id_makan)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('data/dataMakan/Con_Makan/ajax_delete')?>/"+id_makan,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Data Riwayat Penyakit</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">  
          <input type="hidden" value="<?php echo $id;?>" name="nim"/> 
          <div class="form-body">
              
             <div class="form-group">           
              <label class="control-label col-md-3">Menu Sarapan</label>
              <div class="col-md-9">
                <input name="nama_menu_sarapan" placeholder="Menu Sarapan" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Menu Makan Siang</label>
              <div class="col-md-9">
                <input name="nama_menu_makan_siang" placeholder="Menu Makan Siang<" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Menu Makan Malam</label>
              <div class="col-md-9">
                <input name="nama_menu_malam" placeholder="Menu Makan Malam" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Makanan Favorit</label>
              <div class="col-md-9">
                <input name="makanan_favorit" placeholder="Makanan Favorit" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Cemilan Favorit</label>
              <div class="col-md-9">
                <input name="cemilan_favorit" placeholder="Cemilan Favorit" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Tempat Makan</label>
              <div class="col-md-9">
                <input name="tempat_makan" placeholder="Tempat Makan<" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Alamat Warung</label>
              <div class="col-md-9">
                <input name="alamat_warung" placeholder="Alamat Warung" class="form-control" type="text">
              </div>
            </div>

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->