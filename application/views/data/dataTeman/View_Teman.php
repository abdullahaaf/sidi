
  <div class="box box-primary">
    <div class="box-header with-border">
    <h3>Data Teman</h3>
    <br />
    <button class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah Teman</button>
    <br />
    <br />
    <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr class="success">
        <th>No</th>
            <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Alamat</th>
          <th>Tempat Lahir</th>
          <th>Tanggal Lahir</th>
          <th>Nomer Hp</th>
          <th>Email</th>
          <th>Sebagai</th>
          <th style="width:125px;">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>

      <tfoot>
        <tr class="success">
        <th>No</th>
            <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Alamat</th>
          <th>Tempat Lahir</th>
          <th>Tanggal Lahir</th>
          <th>Nomer Hp</th>
          <th>Email</th>
          <th>Sebagai</th>
          <th style="width:125px;">Action</th>
        </tr>
      </tfoot>
    </table>
    </div>
  </div>    
<!-- ============ batas tampil ============ -->

  <script type="text/javascript">

    var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data/dataTeman/Con_Teman/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function add_person()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Teman'); // Set Title to Bootstrap modal title
    }

    function edit_person(id_teman)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('data/dataTeman/Con_Teman/ajax_edit/')?>/" + id_teman,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           
            $('[name="id_teman"]').val(data.id_teman);
            $('[name="namaTeman"]').val(data.namaTeman);
            $('[name="jenisKelamin"]').val(data.jenisKelamin);
            $('[name="alamatTeman"]').val(data.alamatTeman);
            $('[name="tempatLahir"]').val(data.alamatTeman);
            $('[name="tanggalLahir"]').val(data.alamatTeman);
            $('[name="hpTeman"]').val(data.hpTeman);
            $('[name="emailTeman"]').val(data.emailTeman);
            $('[name="sebagaiTeman"]').val(data.sebagaiTeman);
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Teman'); // Set title to Bootstrap modal title
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
          url = "<?php echo site_url('data/dataTeman/Con_Teman/ajax_add')?>";
      }
      else
      {
        url = "<?php echo site_url('data/dataTeman/Con_Teman/ajax_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_person(id_teman)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "<?php echo site_url('data/dataTeman/Con_Teman/ajax_delete')?>/"+id_teman,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

  </script>

<!-- ====================================== Untuk Input Mask ======================================= -->
    <!-- Select2 -->
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Data Teman</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">         
          <input type="hidden" name="id_teman"/> 
          <input type="hidden" value="<?php echo $id;?>" name="nim"/> 
          <div class="form-body">
              
             <div class="form-group">           
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-9">
                <input name="namaTeman" placeholder="Nama" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Jenis Kelamin</label>
              <div class="col-md-9">
                <select name="jenisKelamin" class="form-control">
                  <option value="L">Laki- Laki</option>
                  <option value="P">Perempuan</option>
                </select>
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat</label>
              <div class="col-md-9">
              <textarea name="alamatTeman" placeholder="Alamat" class="form-control" type="text"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Tempat Lahir</label>
              <div class="col-md-9">
                <input name="tempatLahir" placeholder="Tempat Lahir" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Tanggal Lahir</label>
              <div class="col-md-9">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="tanggalLahir" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>
                    </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Nomer Hp</label>
              <div class="col-md-9">
                <input name="hpTeman" placeholder="Nomer HP" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-9">
                <input name="emailTeman" placeholder="Email" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Sebagai</label>
              <div class="col-md-9">
                <select name="sebagaiTeman" class="form-control">
                  <option value="Pacar">Pacar</option>
                  <option value="Teman">Teman</option>
                  <option value="Sahabat">Sahabat</option>
                  <option value="Musuh">Musuh</option>
                </select>
              </div>
            </div>

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->