<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Masuk</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
        <!-- Font Awesome Icons -->
        <link href="<?=base_url('assets/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
        <!-- iCheck -->
        <link href="<?=base_url('assets/plugins/iCheck/square/blue.css');?>" rel="stylesheet" type="text/css" />

    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-box-body">
                <center><p><h3>Masukkan Nim</h3></p></center>
                <form action="<?php echo $action; ?>" method="post">                            
                            <?php echo $error; ?>
                    <div class="form-group has-feedback">
                        <input type="text" name="nim" class="form-control" placeholder="Masukkan NIM"/>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                        </div><!-- /.col -->
                    </div>
                </form>                
                <hr>
                <a href="<?php echo site_url('Daftar'); ?>" class="btn btn-success btn-block btn-flat">Daftar</a><br>
                <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

         <!-- jQuery 2.1.4 -->
        <script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>

        <!-- iCheck -->
        <script src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>       
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>