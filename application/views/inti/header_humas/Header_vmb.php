<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIDI | BINER 2014</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome Icons -->
    <link href="<?=base_url('assets/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?=base_url('assets/plugins/ionicons/css/ionicons.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">
     <!-- jQuery 2.1.4 -->
    <script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.js')?>"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
  <!-- the fixed layout is not compatible with sidebar-mini -->
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b></b>iDi</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>IT_UIN_MLG</b>_2014</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
         
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
             
               
                 
               
                </a>
                <ul class="dropdown-menu">
                 
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="<?=base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
   
              <!-- Tasks: style can be found in dropdown.less -->
             
              <!-- User Account: style can be found in dropdown.less -->
             
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             <a href="<?php echo site_url('data/Cnim/Logout');?>" class="btn btn-warning btn-flat">Keluar</a>
            </ul>
          </div>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
<!--            <li class="header">MAIN NAVIGATION</li>-->


              <li class="active"><a href="#">
                <i class="glyphicon glyphicon-education"></i>
                <span>Data  Text</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
                      <ul class="treeview-menu active">
                       <li class="active"><a href='<?php echo site_url('data/datadiri/Con_Master_Data_Datadiri')?>'></i> Data Diri</a></li>
                      <li class="active"><a href='<?php echo site_url('data/datapendidikan/Con_Master_Data_Pendidikan')?>'></i>Riwayat Pendidikan</a></li>
                     
                      

     

      <li class="active"><a href='<?php echo site_url('data/dataPenyakit/Con_Riwayat_Penyakit')?>'></i> Data Riwayat Penyakit</a></li>


                      <li class="active"><a href='<?php echo site_url('data/dataMakan/Con_Makan')?>'>Kebiasaan Makan</a></li>
                   
                     
  <li class="active"><a href='<?php echo site_url('data/dataTeman/Con_Teman')?>'></i> Sahabat Terdekat</a></li>

              </ul>
</li>
<li class="active"><a href="#">
                <i class="glyphicon glyphicon-education"></i>
                <span> Data Upload Foto</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu active">
                  <li class="active"><a href='<?php echo site_url('Images_examples')?>'></i> Edit dan View</a></li>
                  <li class="active"><a href='<?php echo site_url('Images_examples/indexDropZone')?>'></i> Upload Drag & Drop</a></li>
                  
                   <li class="active"><a href='<?php echo site_url('data/video/Cvideo')?>'></i>Putar Video</a></li>
                  
              </ul>
</li>


              
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>




      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
