<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIDI | BINER 2014</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome Icons -->
    <link href="<?=base_url('assets/plugins/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?=base_url('assets/plugins/ionicons/css/ionicons.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">
     <!-- jQuery 2.1.4 -->
    <script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.js')?>"></script>

  </head>
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
       <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content">
          <div class="box box-primary">
    <div class="box-header with-border">
    <h1>Silahkan Input Pendaftaran</h1>

    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
    <!-- InputMask -->
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
    <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

   <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>
        <form action="#" id="form" class="form-horizontal">         
          <!-- <input type="hidden" value="" name="id_datadiri"/>  -->
          <div class="form-body">
              <div class="form-group">
              <label class="control-label col-md-3">NIM</label>
              <div class="col-md-9">
                <input name="nim" placeholder="NIM" class="form-control" type="text">
              </div>
              </div>

             <div class="form-group">           
              <label class="control-label col-md-3">Nama</label>
              <div class="col-md-9">
                <input name="nama" placeholder="nama" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">           
              <label class="control-label col-md-3">Alamat</label>
              <div class="col-md-9">
                <input name="alamat" placeholder="alamat" class="form-control" type="text">
              </div>
            </div>
            
              
              <div class="form-group">
              <label class="control-label col-md-3">Telepon</label>
              <div class="col-md-9">
                <input name="telepon" placeholder="telepon" class="form-control" type="text">
              </div>
            </div>
              
            <div class="form-group">
              <label class="control-label col-md-3">Hp</label>
              <div class="col-md-9">
                <input name="hp" placeholder="hp" class="form-control" type="text">
              </div>
              </div>
                
                <div class="form-group">
              <label class="control-label col-md-3">email</label>
              <div class="col-md-9">
                <input name="email" placeholder="email" class="form-control" type="text">
              </div>
            </div>             
               <div class="form-group">
              <label class="control-label col-md-3">Tanggal Lahir</label>
              <div class="col-md-9">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="tanggal_lahir" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask>
                    </div><!-- /.input group -->

              </div>
            </div>
                
                <div class="form-group">
              <label class="control-label col-md-3">Kebangsaan</label>
              <div class="col-md-9">
                <input name="kebangsaan" placeholder="kebangsaan" class="form-control" type="text">
              </div>
            </div>
                <div class="form-group">
              <label class="control-label col-md-3">Agama</label>
              <div class="col-md-9">
                <input name="agama" placeholder="agama" class="form-control" type="text">
              </div>
            </div>
                
                <div class="form-group">
              <label class="control-label col-md-3">Jenis Kelamin</label>
              <div class="col-md-9">
                <select name="jenis_kelamin" class="form-control">
                   <option value="laki-laki">Laki laki</option>
                   <option value="perempuan">Perempuan</option></select>
                  
                 
              </div>
            </div>
              
              <div class="form-group">
              <label class="control-label col-md-3">Status</label>
              <div class="col-md-9">
               <input name="status" placeholder="Status" class="form-control" type="text">
              </div>
            </div>
              
                <div class="form-group">
              <label class="control-label col-md-3">Hobi</label>
              <div class="col-md-9">
               <input name="hobi" placeholder="Hobi" class="form-control" type="text">
              </div>
            </div>
             
          </div>
          <div class="modal-footer">
            <button type="submit" onclick="save()"class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </form>
          </div>
        </div>
        </section>
  <script type="text/javascript">
  function save()
    {
      var url;
        url = "<?php echo site_url('Daftar/ajax_add')?>";
      
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
             
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              window.location.href="<?php echo site_url('data/Cnim')?>";
            }
        });
    }
    </script>
      </div>
    </div>
  </body>