<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Upload_Img extends CI_Controller {

  

  public function __construct() {

  parent::__construct();

   $this->load->helper(array('url','html','form'));

  }

  

  public function index() {

   $this->load->view('uploadimg');

  }

  

  public function upload() {

   if (!empty($_FILES)) {

    $tempFile = $_FILES['file']['tmp_name'];

    $fileName = $_FILES['file']['name'];

    $fileType = $_FILES['file']['type'];

    $fileSize = $_FILES['file']['size'];

    $targetPath = './assets/uploads/';

    $targetFile = $targetPath . $fileName ;

    move_uploaded_file($tempFile, $targetFile);

    // jika anda ingin menyimpannya ke dalam database

    // berikut adalah contoh model sederhana untuk menyimpan kdalam database.

    

    // sintak untuk menyimpan ke database :

    $this->db->insert('example_4',array('type' => $fileType, 'url' => $fileName, 'ukuran' => $fileSize));

     }

  }

 }

?>