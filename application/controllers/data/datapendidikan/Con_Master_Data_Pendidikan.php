<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Data_Pendidikan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('data/datapendidikan/Model_Data_Pen','model');
    }

    public function index()
    {
        $this->load->helper('url');         
        $sessionData = $this->session->userdata('nim');
        $data['id'] = $sessionData;
        $this->load->view('inti/header_humas/Header_vmb');
        $this->load->view('data/datapendidikan/View_Data_Pen',$data);        
        $this->load->view('inti/Footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
//            $row[] = $model->id_pendidikan;
            $row[] = $model->nama_mi;
            $row[] = $model->alamat_mi;
            $row[] = $model->thn_lulus_mi;
            $row[] = $model->nama_smp;
            $row[] = $model->alamat_smp;
            $row[] = $model->thn_lulus_smp;
            $row[] = $model->nama_sma;
            $row[] = $model->alamat_sma;
            $row[] = $model->thn_lulus_sma;
            $row[] = $model->nama_pt;
            $row[] = $model->alamat_pt;            
            $row[] = $model->thn_lulus;
          
            
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id_pendidikan."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id_pendidikan."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id_pendidikan)
    {
        $data = $this->model->get_by_id($id_pendidikan);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
            'nim' => $this->input->post('nim'),
                'nama_mi' => $this->input->post('nama_mi'),
                'alamat_mi' => $this->input->post('alamat_mi'),
                'thn_lulus_mi' => $this->input->post('thn_lulus_mi'),
                'nama_smp' => $this->input->post('nama_smp'),
                'alamat_smp' => $this->input->post('alamat_smp'),
            'thn_lulus_smp' => $this->input->post('thn_lulus_smp'),
            'nama_sma' => $this->input->post('nama_sma'),
            'alamat_sma' => $this->input->post('alamat_sma'),
            'thn_lulus_sma' => $this->input->post('thn_lulus_sma'),
            'nama_pt' => $this->input->post('nama_pt'),
            'alamat_pt' => $this->input->post('alamat_pt'),
            'thn_lulus' => $this->input->post('thn_lulus'),

            
            
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                    'nama_mi' => $this->input->post('nama_mi'),
                'alamat_mi' => $this->input->post('alamat_mi'),
                'thn_lulus_mi' => $this->input->post('thn_lulus_mi'),
                'nama_smp' => $this->input->post('nama_smp'),
                'alamat_smp' => $this->input->post('alamat_smp'),
            'thn_lulus_smp' => $this->input->post('thn_lulus_smp'),
            'nama_sma' => $this->input->post('nama_sma'),
            'alamat_sma' => $this->input->post('alamat_sma'),
            'thn_lulus_sma' => $this->input->post('thn_lulus_sma'),
            'nama_pt' => $this->input->post('nama_pt'),
            'alamat_pt' => $this->input->post('alamat_pt'),
            'thn_lulus' => $this->input->post('thn_lulus'),

             'id_pendidikan' => $this->input->post('id_pendidikan'),
            
            );
        $this->model->update(array('id_pendidikan' => $this->input->post('id_pendidikan')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_pendidikan)
    {
        $this->model->delete_by_id($id_pendidikan);
        echo json_encode(array("status" => TRUE));
    }

}
