<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Humas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('humas/model_master_humas','model');
    }

    public function index()
    {
        $this->load->helper('url');        
        $this->load->view('inti/header');
        $this->load->view('humas/view_master_humas');        
        // echo "<h1> TAMPILAN HUMAS </h1>";        
        $this->load->view('inti/footer');
    }

    // awal untuk mengetes saja
    public function backupviewcrud()
    {
        $this->load->view('isi/backupviewcrud');
    }
    // selesai untuk mengetes

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $model->namaKegiatan;
            $row[] = $model->pukul;
            $row[] = $model->tempat;
            $row[] = $model->tanggal;
            // $row[] = $concrud->dob;

            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->nomor."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->nomor."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($nomor)
    {
        $data = $this->model->get_by_id($nomor);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'namaKegiatan' => $this->input->post('namaKegiatan'),
                'pukul' => $this->input->post('pukul'),
                'tempat' => $this->input->post('tempat'),
                'tanggal' => $this->input->post('tanggal'),
                // 'dob' => $this->input->post('dob'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'namaKegiatan' => $this->input->post('namaKegiatan'),
                'pukul' => $this->input->post('pukul'),
                'tempat' => $this->input->post('tempat'),
                'tanggal' => $this->input->post('tanggal'),
                // 'dob' => $this->input->post('dob'),
            );
        $this->model->update(array('nomor' => $this->input->post('nomor')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($nomor)
    {
        $this->model->delete_by_id($nomor);
        echo json_encode(array("status" => TRUE));
    }

}
