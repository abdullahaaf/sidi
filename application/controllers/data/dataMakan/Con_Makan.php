<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Makan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('data/Cnim'));
            }
        $this->load->model('data/dataMakan/Model_Makan','model');
    }

    public function index()
    {
        $this->load->helper('url');      
            $sessionData = $this->session->userdata('nim');
            $data['id'] = $sessionData;   
        $this->load->view('inti/header_humas/Header_vmb',$data);
        $this->load->view('data/dataMakan/View_Makan');        
        $this->load->view('inti/Footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $urut= 1;
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $urut;
            $row[] = $model->nama_menu_sarapan;
            $row[] = $model->nama_menu_makan_siang;
            $row[] = $model->nama_menu_malam;
            $row[] = $model->makanan_favorit;
            $row[] = $model->cemilan_favorit;
            $row[] = $model->tempat_makan;
            $row[] = $model->alamat_warung;

            
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id_makan."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id_makan."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
        
            $data[] = $row;
            $urut++;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id_makan)
    {
        $data = $this->model->get_by_id($id_makan);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'nim' => $this->input->post('nim'),
                'nama_menu_sarapan' => $this->input->post('nama_menu_sarapan'),
                'nama_menu_makan_siang' => $this->input->post('nama_menu_makan_siang'),
                'nama_menu_malam' => $this->input->post('nama_menu_malam'),
                'makanan_favorit' => $this->input->post('makanan_favorit'),
                'cemilan_favorit' => $this->input->post('cemilan_favorit'),
                'tempat_makan' => $this->input->post('tempat_makan'),
                'alamat_warung' => $this->input->post('alamat_warung'),
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
               'nama_menu_sarapan' => $this->input->post('nama_menu_sarapan'),
                'nama_menu_makan_siang' => $this->input->post('nama_menu_makan_siang'),
                'nama_menu_malam' => $this->input->post('nama_menu_malam'),
                'makanan_favorit' => $this->input->post('makanan_favorit'),
                'cemilan_favorit' => $this->input->post('cemilan_favorit'),
                'tempat_makan' => $this->input->post('tempat_makan'),
                'alamat_warung' => $this->input->post('alamat_warung'),
            );
        $this->model->update(array('id_makan' => $this->input->post('id_makan')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_makan)
    {
        $this->model->delete_by_id($id_makan);
        echo json_encode(array("status" => TRUE));
    }

}
