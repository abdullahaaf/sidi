<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Teman extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('data/dataTeman/Model_Teman','model');
    }

    public function index()
    {
        $this->load->helper('url');
        $sessionData = $this->session->userdata('nim');
        $data['id'] = $sessionData;         
        $this->load->view('inti/header_humas/Header_vmb');
        $this->load->view('data/dataTeman/View_Teman',$data);        
        $this->load->view('inti/Footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $urut= 1;
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $urut;
            $row[] = $model->namaTeman;
            $row[] = $model->jenisKelamin;
            $row[] = $model->alamatTeman;
            $row[] = $model->tempatLahir;
            $row[] = $model->tanggalLahir;
            $row[] = $model->hpTeman;
            $row[] = $model->emailTeman;
            $row[] = $model->sebagaiTeman;
            
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id_teman."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id_teman."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
        
            $data[] = $row;
            $urut++;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id_teman)
    {
        $data = $this->model->get_by_id($id_teman);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'nim' => $this->input->post('nim'),
                'namaTeman' => $this->input->post('namaTeman'),
                'jenisKelamin' => $this->input->post('jenisKelamin'),
                'alamatTeman' => $this->input->post('alamatTeman'),
                'tempatLahir' => $this->input->post('tempatLahir'),
                'tanggalLahir' => $this->input->post('tanggalLahir'),
                'hpTeman' => $this->input->post('hpTeman'),
                'emailTeman' => $this->input->post('emailTeman'),
                'sebagaiTeman' => $this->input->post('sebagaiTeman'),
            
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
               'namaTeman' => $this->input->post('namaTeman'),
                'jenisKelamin' => $this->input->post('jenisKelamin'),
                'alamatTeman' => $this->input->post('alamatTeman'),
                'tempatLahir' => $this->input->post('tempatLahir'),
                'tanggalLahir' => $this->input->post('tanggalLahir'),
                'hpTeman' => $this->input->post('hpTeman'),
                'emailTeman' => $this->input->post('emailTeman'),
                'sebagaiTeman' => $this->input->post('sebagaiTeman'),
            );
        $this->model->update(array('id_teman' => $this->input->post('id_teman')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_teman)
    {
        $this->model->delete_by_id($id_teman);
        echo json_encode(array("status" => TRUE));
    }

}
