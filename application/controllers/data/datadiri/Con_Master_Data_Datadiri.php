<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Data_Datadiri extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('data/Cnim'));
            }
        $this->load->model('data/datadiri/Model_Master_Data_Datadiri','model');
    }

    public function index()
    {
            $this->load->helper('url');         

            $sessionData = $this->session->userdata('nim');
            $data['id'] = $sessionData;
            $this->load->view('inti/header_humas/Header_vmb');
            $this->load->view('data/datadiri/View_Master_Data_Datadiri',$data);        
            $this->load->view('inti/Footer');
    }

    public function ajax_edit($id_datadiri)
    {
        $data = $this->model->get_by_id($id_datadiri);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
            'nim' => $this->input->post('nim'),
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('telepon'),
            'hp' => $this->input->post('hp'),
            'email' => $this->input->post('email'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'kebangsaan' => $this->input->post('kebangsaan'),
            'agama' => $this->input->post('agama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'status' => $this->input->post('status'),
            'hobi' => $this->input->post('hobi'),
            
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
               'nim' => $this->input->post('nim'),
               'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'telepon' => $this->input->post('telepon'),
                'hp' => $this->input->post('hp'),
                'email' => $this->input->post('email'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'kebangsaan' => $this->input->post('kebangsaan'),
            'agama' => $this->input->post('agama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'status' => $this->input->post('status'),
            'hobi' => $this->input->post('hobi'),
            );
        $this->model->update(array('nim' => $this->input->post('nim')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_datadiri)
    {
        $this->model->delete_by_id($id_datadiri);
        echo json_encode(array("status" => TRUE));
    }

}
