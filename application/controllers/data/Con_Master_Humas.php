<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Master_Humas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
// jika elum login redirect ke login
        if ($this->session->userdata('logged')<>1)
        {
            redirect(site_url('humas/Con_Auth'));
        }
    }

    public function index()
    {
        $this->load->helper('url');         
        $this->load->view('inti/header_humas/header_vmb');
        $this->load->view('humas/View_Dashboard_Humas');        
        $this->load->view('inti/Footer');
    }
}

