<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cnim extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index($error = NULL) {
        $data = array(
            'action' => site_url('data/Cnim/login'),//lokasi di controller/data
            'error' => $error
        );
        $this->load->view('data/Vnim',$data);
    }

    public function login() {
        $this->load->model('data/Mnim','model');
        $login = $this->model->login($this->input->post('nim'));

        if ($login == 1) {
            $row = $this->model->data_login($this->input->post('nim'));

            $data = array(
                'logged' => TRUE,
                'nim' => $row->nim
            );
            $this->session->set_userdata($data);

            redirect(site_url('data/datadiri/Con_Master_Data_Datadiri'));
        } else {
            $error = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong><center>Bahaya!</center></strong><center>Masukkan NIM dengan benar</center></div>';
            $this->index($error);
        }
    }

    function logout() {
        $this->session->sess_destroy();
        
        redirect(site_url('data/Cnim'));
    }
}
