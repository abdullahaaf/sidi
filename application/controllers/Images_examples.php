<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images_examples extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		/* Standard Libraries */
		$this->load->database();
		/* ------------------ */
		
		// $this->load->helper('url'); //Just for the examples, this is not required thought for the library
		$this->load->helper(array('url','html','form'));
		$this->load->library('Image_CRUD');
	}

	// function view_gambar()
	// {
	// 	$sessionData = $this->session->userdata('nim');
 //        $data['id'] = $sessionData;
	// 	$this->load->view('inti/header_humas/Header_vmb',$data);
	// 	$this->load->view('example.php',$output);
	// 	$this->load->view('inti/Footer');	
	// }
	
	function _example_output($output = null)
	{	
		$this->load->model('data/dataTeman/Model_Teman','model');
		$sessionData = $this->session->userdata('nim');
        $data['id'] = $sessionData;

		$this->load->view('inti/header_humas/Header_vmb',$data);
		$this->load->view('example.php',$output);
		$this->load->view('inti/Footer');	
	}

	function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	

	function example4()
	{
		$viewFile=$this->input->post('viewFile');
		$image_crud = new image_CRUD();
		$image_crud->unset_upload();
		$image_crud->unset_delete();
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('url');
		$image_crud->set_title_field('title');
		$image_crud->set_table($viewFile)
		->set_ordering_field('priority')
		->set_image_path('assets/uploads'); //Folder tempat upload
			
		$output = $image_crud->render();
	
		// $this->_example_output($output);
		$this->_example_output($output);
	}
	
	function simple_photo_gallery()
	{
		$image_crud = new image_CRUD();
		$viewFile=$this->input->post('viewFile');
		$image_crud->unset_upload();
		$image_crud->unset_delete();
		
		$image_crud->set_primary_key_field('id');
		$image_crud->set_url_field('url');
		$image_crud->set_table($viewFile)->set_image_path('assets/uploads'); //Folder tempat upload
		
		$output = $image_crud->render();
		
		$this->_example_output($output);		
	}

	function indexDropZone(){
		$this->load->view('inti/header_humas/Header_vmb');
		$sessionData = $this->session->userdata('nim');
        $data['id'] = $sessionData;
		$this->load->view('uploadimg',$data);
		$this->load->view('inti/Footer');	
	}

	public function upload() {

   		if (!empty($_FILES)) {		
	    $tempFile = $_FILES['file']['tmp_name'];
    	$fileName = $_FILES['file']['name'];
    	$fileType = $_FILES['file']['type'];
    	$fileSize = $_FILES['file']['size'];
    	$targetPath = './assets/uploads/';
    	$targetFile = $targetPath . $fileName ;
    	// $this->importdata->tb;
    	$tb=$this->input->post('tb1');
    	$nim=$this->session->userdata('nim');;

    	move_uploaded_file($tempFile, $targetFile);
    	// sintak untuk menyimpan ke database :
    	$this->db->insert($tb,array('nim' => $nim,'type' => $fileType, 'title' => $fileName, 'ukuran' => $fileSize, 'url' => $fileName));

    	// $this->db->insert('example_4',array('type' => $fileType, 'title' => $fileName, 'ukuran' => $fileSize, 'url' => $fileName));


     	}

  }
}