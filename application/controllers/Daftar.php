<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Daftar extends CI_Controller {

  

  public function __construct() {

  parent::__construct();

   $this->load->model('Mdaftar','model');

  }

  public function index() {
    $this->load->helper('url'); 
    $this->load->view('Daftar');        
    $this->load->view('inti/Footer');
   }	

    public function ajax_add()
    {
        $data = array(
            'nim' => $this->input->post('nim'),
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('telepon'),
            'hp' => $this->input->post('hp'),
            'email' => $this->input->post('email'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'kebangsaan' => $this->input->post('kebangsaan'),
            'agama' => $this->input->post('agama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'status' => $this->input->post('status'),
            'hobi' => $this->input->post('hobi'),
            
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }
}