<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_Riwayat_Penyakit extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('data/dataPenyakit/Model_Riwayat_Penyakit','model');
    }

    public function index()
    {
        $this->load->helper('url');         
        $this->load->view('inti/header_humas/Header_vmb');
        $this->load->view('data/dataPenyakit/View_Riwayat_Penyakit');        
        $this->load->view('inti/Footer');
    }

    public function ajax_list()
    {
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $urut= 1;
        foreach ($list as $model) {
            $no++;
            $row = array();
            $row[] = $urut;
            $row[] = $model->gol_darah;
            $row[] = $model->penyakit_yg_pernah_diderita;
            $row[] = $model->penyakit_sedang_diderita;
            
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id_riwayat."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id_riwayat."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
        
            $data[] = $row;
            $urut++;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model->count_all(),
                        "recordsFiltered" => $this->model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id_riwayat)
    {
        $data = $this->model->get_by_id($id_riwayat);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'gol_darah' => $this->input->post('gol_darah'),
                'penyakit_yg_pernah_diderita' => $this->input->post('penyakit_yg_pernah_diderita'),
                'penyakit_sedang_diderita' => $this->input->post('penyakit_sedang_diderita'),
            
            );
        $insert = $this->model->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
               'gol_darah' => $this->input->post('gol_darah'),
                'penyakit_yg_pernah_diderita' => $this->input->post('penyakit_yg_pernah_diderita'),
                'penyakit_sedang_diderita' => $this->input->post('penyakit_sedang_diderita'),
            );
        $this->model->update(array('id_riwayat' => $this->input->post('id_riwayat')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id_riwayat)
    {
        $this->model->delete_by_id($id_riwayat);
        echo json_encode(array("status" => TRUE));
    }

}
