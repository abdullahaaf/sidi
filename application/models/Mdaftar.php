<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdaftar extends CI_Model {

	var $table = 'data_diri';
	var $column = array('nim','nama','alamat','telepon','hp','email','tanggal_lahir','kebangsaan','agama','jenis_kelamin','status','hobi');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
}
