<?php
 class Gallery_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function fetch_image($path)
    {
        $this->load->helper(‘file’);
        return get_filenames($path);
    }
 }