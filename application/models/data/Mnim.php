<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnim extends CI_Model {

	//    untuk mengcek jumlah username dan password yang sesuai
    function login($nim) {
        $this->db->where('nim', $nim);
        $query =  $this->db->get('data_diri');
        return $query->num_rows();
    }
    
//    untuk mengambil data hasil login
    function data_login($nim) {
        $this->db->where('nim', $nim);
        return $this->db->get('data_diri')->row();
    }

}
