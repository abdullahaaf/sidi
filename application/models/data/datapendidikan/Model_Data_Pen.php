<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Data_Pen extends CI_Model {

	var $table = 'text_pendidikan';
	var $column = array('nama_mi','alamat_mi','thn_lulus_mi','nama_smp',
		'alamat_smp','thn_lulus_smp','nama_sma','alamat_sma',
		'thn_lulus_sma','nama_pt','alamat_pt',
		'thn_lulus'

		);
	var $order = array('id_pendidikan' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id_pendidikan)
	{
		$this->db->from($this->table);
		$this->db->where('nim',$id_pendidikan);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_pendidikan)
	{
		$this->db->where('id_pendidikan', $id_pendidikan);
		$this->db->delete($this->table);
	}


}
