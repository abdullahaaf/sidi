<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvideo extends CI_Model {

	var $table = 'video';
	var $column = array('title','url','priority','type','ukuran');
	var $order = array('nim' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function urlVideo()
	{
		$this->db->select('url,type,title');
        $this->db->from($this->table);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

}
